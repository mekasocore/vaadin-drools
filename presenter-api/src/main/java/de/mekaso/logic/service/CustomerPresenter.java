package de.mekaso.logic.service;

import de.mekaso.logic.model.Customer;

public interface CustomerPresenter {
	public void validate(Customer customer);	
}
