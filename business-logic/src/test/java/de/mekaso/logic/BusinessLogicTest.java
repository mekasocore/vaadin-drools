package de.mekaso.logic;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import static org.hamcrest.CoreMatchers.*;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import de.mekaso.logic.model.Customer;
import de.mekaso.logic.model.ValidationResult;
import de.mekaso.logic.model.ValidationResult.ValidationState;
import de.mekaso.logic.util.RuleFilter;

public class BusinessLogicTest {

	private KieSession kSession;
	
	@Before
	public void init() {
		KieServices ks = KieServices.Factory.get();
	    KieContainer kContainer = ks.getKieClasspathContainer();
    	kSession = kContainer.newKieSession("customer-rules");
	}
	
	@Test
	public void testValidationFailure() {
		Customer customer = new Customer();
		this.kSession.insert(customer);
		ValidationResult validationResult = ValidationResult.createValidationResult();
		this.kSession.insert(validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData")));
		assertNotNull(validationResult.getMessages());
		assertThat(validationResult.getValidationState(), is(equalTo(ValidationState.FAILURE)));
	}
	
	@Test
	public void testValidationFailureEmpty() {
		Customer customer = new Customer();
		customer.setPhone("");
		customer.setEmail("");
		this.kSession.insert(customer);
		ValidationResult validationResult = ValidationResult.createValidationResult();
		this.kSession.insert(validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData")));
		assertNotNull(validationResult.getMessages());
		assertThat(validationResult.getValidationState(), is(equalTo(ValidationState.FAILURE)));
	}
	
	@Test
	public void testValidationSuccess() {
		Customer customer = new Customer();
		customer.setPhone("12345");
		this.kSession.insert(customer);
		ValidationResult validationResult = ValidationResult.createValidationResult();
		this.kSession.insert(validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData")));
		assertThat(validationResult.getValidationState(), is(equalTo(ValidationState.SUCCESS)));
		assertNull(validationResult.getMessages());
	}
	
	@Test
	public void testValidationMulti() {
		Customer customer = new Customer();
		FactHandle customerHandle = this.kSession.insert(customer);
		ValidationResult validationResult = ValidationResult.createValidationResult();
		FactHandle validationResultHandle = this.kSession.insert(validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData")));
		assertNotNull(validationResult.getMessages());
		assertThat(validationResult.getValidationState(), is(equalTo(ValidationState.FAILURE)));
		
//		this.kSession.delete(customerHandle);
//		this.kSession.delete(validationResultHandle);

		customer = new Customer();
		customer.setPhone("12345");

		this.kSession.update(customerHandle, customer);
		validationResult = ValidationResult.createValidationResult();
		this.kSession.update(validationResultHandle, validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData")));
		assertThat(validationResult.getValidationState(), is(equalTo(ValidationState.SUCCESS)));
		assertNull(validationResult.getMessages());
	}
}
