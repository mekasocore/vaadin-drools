package de.mekaso.logic.util;

import java.util.List;

import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;

public class RuleFilter {

	public static AgendaFilter applyRules(final List<String> ruleNameList) {
		AgendaFilter filter = new AgendaFilter() {
			@Override
			public boolean accept(Match match) {
				String ruleName = match.getRule().getName();
				return ruleNameList.contains(ruleName);
			}
		};
		return filter;
	}
}
