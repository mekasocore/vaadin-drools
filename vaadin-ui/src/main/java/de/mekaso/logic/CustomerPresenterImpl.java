package de.mekaso.logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.kie.api.cdi.KSession;
import org.kie.api.command.Command;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.command.CommandFactory;

import com.vaadin.cdi.NormalViewScoped;

import de.mekaso.logic.event.Presenter;
import de.mekaso.logic.event.PresenterType;
import de.mekaso.logic.model.Customer;
import de.mekaso.logic.model.ValidationResult;
import de.mekaso.logic.model.ValidationResult.ValidationState;
import de.mekaso.logic.service.CustomerPresenter;
import de.mekaso.logic.util.RuleFilter;

@NormalViewScoped
public class CustomerPresenterImpl implements CustomerPresenter {

	@Inject
	private CustomerView view;
	
	@Inject
	private CustomerModel model;
	
	@Inject
	@KSession("customer-rules")
	private KieSession kSession;
	
	public void init(@Observes @PresenterType(Presenter.Customer) Presenter presenter) {
		this.view.initForm(this.model.getCustomer());
	}

	@Override
	public void validate(@Observes Customer customer) {
		ValidationResult validationResult = ValidationResult.createValidationResult();
		FactHandle customerHandle = this.kSession.insert(customer);
		FactHandle resultHandle = this.kSession.insert(validationResult);
		this.kSession.fireAllRules(RuleFilter.applyRules(Arrays.asList("Customer.contactData", "Customer.validEmail")));
		if (validationResult.getValidationState() == ValidationState.FAILURE) {
			this.view.showMessage(validationResult.getMessages());
		}
		this.kSession.delete(customerHandle);
		this.kSession.delete(resultHandle);
	}
}