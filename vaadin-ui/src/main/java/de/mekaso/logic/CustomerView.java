package de.mekaso.logic;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.vaadin.cdi.CDIView;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.converter.LocalDateToDateConverter;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.mekaso.logic.event.Presenter;
import de.mekaso.logic.event.PresenterQualifier;
import de.mekaso.logic.model.Customer;
import de.mekaso.logic.model.Gender;
import de.mekaso.logic.model.ValidationMessage;

@CDIView("")
public class CustomerView extends CustomComponent implements View {
	private static final long serialVersionUID = 1L;
	private CustomerViewLayout layout;
	private Binder<Customer> binder;
	
	@Inject
	private javax.enterprise.event.Event<Presenter> eventProducer;
	
	@Inject
	private javax.enterprise.event.Event<Customer> customerEventProducer;
	
	@PostConstruct
	public void init() {
		this.layout = new CustomerViewLayout();
	}
	
	@Override
	public void enter(ViewChangeEvent vce) {
		this.layout.getGender().setItems(Gender.values());
		super.setCompositionRoot(this.layout.getRootFormLayout());
		this.eventProducer.select(new PresenterQualifier(Presenter.Customer)).fire(Presenter.Customer);
		this.layout.getSubmitButton().addClickListener(event -> {
			final Customer customer = this.binder.getBean();
			try {
				this.binder.writeBean(customer);
				this.customerEventProducer.fire(customer);
			} catch (ValidationException e) {
				Notification.show("Error", e.getMessage(), Notification.Type.ERROR_MESSAGE);
			}			
		});
		
		this.layout.getGender().addValueChangeListener(event -> {
			
		});
	}
	
	public void showMessage(List<ValidationMessage> messages) {
		StringBuilder description = new StringBuilder();
		messages.forEach(message -> {
			description.append(message.getMessage());
			}
		);
		Notification.show("Validation", description.toString() , Notification.Type.ERROR_MESSAGE);
	}
	
	public void initForm(Customer customer) {
		this.binder = new Binder<>();
		this.binder.bind(this.layout.getFirstname(), Customer::getFirstname, Customer::setFirstname);
		this.binder.bind(this.layout.getName(), Customer::getName, Customer::setName);
		this.binder.forField(this.layout.getBirthday())
		.withConverter(new LocalDateToDateConverter())
		.bind(Customer::getBirthday, Customer::setBirthday);
		this.binder.bind(this.layout.getGender(), Customer::getGender, Customer::setGender);
		this.binder.bind(this.layout.getEmail(), Customer::getEmail, Customer::setEmail);
		this.binder.bind(this.layout.getPhone(), Customer::getPhone, Customer::setPhone);
		this.binder.setBean(customer);
	}

}
