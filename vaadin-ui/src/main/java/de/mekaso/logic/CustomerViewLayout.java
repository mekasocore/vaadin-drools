package de.mekaso.logic;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Layout.AlignmentHandler;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;

import de.mekaso.logic.model.Gender;

public class CustomerViewLayout {
    
    protected final FormLayout rootFormLayout = new FormLayout();
    protected final TextField name = new TextField();
    protected final TextField firstname = new TextField();
    protected final DateField birthday = new DateField();
    protected final RadioButtonGroup<Gender> gender = new RadioButtonGroup<Gender>();
    protected final TextField phone = new TextField();
    protected final TextField email = new TextField();
    protected final Button submitButton = new Button();
    
    public CustomerViewLayout() {
        super();
        reset();
    }
    
    public synchronized void reset() {
        rootFormLayout.removeAllComponents();
        rootFormLayout.addComponent(name);
        rootFormLayout.addComponent(firstname);
        rootFormLayout.addComponent(birthday);
        rootFormLayout.addComponent(gender);
        rootFormLayout.addComponent(phone);
        rootFormLayout.addComponent(email);
        rootFormLayout.addComponent(submitButton);
        initRootFormLayout();
        initSubmitButton();
        initEmail();
        initPhone();
        initGender();
        initBirthday();
        initFirstname();
        initName();
    }
    
    protected void initName() {
        name.setValue("");
        name.setCaption("Name");
        name.addStyleName("");
        name.setEnabled(true);
        name.setVisible(true);
        name.setReadOnly(false);
        name.setWidth("100.0%");
        name.setHeight("-1.0px");
        name.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) name.getParent();
        parentAbstractOrderedLayout.setExpandRatio(name, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) name.getParent();
        parentAlignmentHandler.setComponentAlignment(name, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getName() {
        return name;
    }
    
    protected void initFirstname() {
        firstname.setValue("");
        firstname.setCaption("Firstname");
        firstname.addStyleName("");
        firstname.setEnabled(true);
        firstname.setVisible(true);
        firstname.setReadOnly(false);
        firstname.setWidth("100.0%");
        firstname.setHeight("-1.0px");
        firstname.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) firstname.getParent();
        parentAbstractOrderedLayout.setExpandRatio(firstname, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) firstname.getParent();
        parentAlignmentHandler.setComponentAlignment(firstname, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getFirstname() {
        return firstname;
    }
    
    protected void initBirthday() {
        birthday.setCaption("Birthday");
        birthday.addStyleName("");
        birthday.setEnabled(true);
        birthday.setVisible(true);
        birthday.setReadOnly(false);
        birthday.setWidth("100.0%");
        birthday.setHeight("-1.0px");
        birthday.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) birthday.getParent();
        parentAbstractOrderedLayout.setExpandRatio(birthday, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) birthday.getParent();
        parentAlignmentHandler.setComponentAlignment(birthday, Alignment.TOP_LEFT);
    }
    
    
    public final DateField getBirthday() {
        return birthday;
    }
    
    protected void initGender() {
        gender.setHtmlContentAllowed(false);
        gender.setCaption("Gender");
        gender.addStyleName("");
        gender.setEnabled(true);
        gender.setVisible(true);
        gender.setReadOnly(false);
        gender.setWidth("100.0%");
        gender.setHeight("-1.0px");
        gender.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) gender.getParent();
        parentAbstractOrderedLayout.setExpandRatio(gender, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) gender.getParent();
        parentAlignmentHandler.setComponentAlignment(gender, Alignment.TOP_LEFT);
    }
    
    
    public final RadioButtonGroup<Gender> getGender() {
        return gender;
    }
    
    protected void initPhone() {
        phone.setValue("");
        phone.setCaption("Phone");
        phone.addStyleName("");
        phone.setEnabled(true);
        phone.setVisible(true);
        phone.setReadOnly(false);
        phone.setWidth("100.0%");
        phone.setHeight("-1.0px");
        phone.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) phone.getParent();
        parentAbstractOrderedLayout.setExpandRatio(phone, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) phone.getParent();
        parentAlignmentHandler.setComponentAlignment(phone, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getPhone() {
        return phone;
    }
    
    protected void initEmail() {
        email.setValue("");
        email.setCaption("E-Mail");
        email.addStyleName("");
        email.setEnabled(true);
        email.setVisible(true);
        email.setReadOnly(false);
        email.setWidth("100.0%");
        email.setHeight("-1.0px");
        email.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) email.getParent();
        parentAbstractOrderedLayout.setExpandRatio(email, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) email.getParent();
        parentAlignmentHandler.setComponentAlignment(email, Alignment.TOP_LEFT);
    }
    
    
    public final TextField getEmail() {
        return email;
    }
    
    protected void initSubmitButton() {
        submitButton.setCaption("Submit");
        submitButton.addStyleName("");
        submitButton.setEnabled(true);
        submitButton.setVisible(true);
        submitButton.setWidth("-1.0px");
        submitButton.setHeight("-1.0px");
        submitButton.setDisableOnClick(false);
        submitButton.setTabIndex(0);
        AbstractOrderedLayout parentAbstractOrderedLayout = (AbstractOrderedLayout) submitButton.getParent();
        parentAbstractOrderedLayout.setExpandRatio(submitButton, Double.valueOf(0.0).floatValue());
        AlignmentHandler parentAlignmentHandler = (AlignmentHandler) submitButton.getParent();
        parentAlignmentHandler.setComponentAlignment(submitButton, Alignment.TOP_LEFT);
    }
    
    
    public final Button getSubmitButton() {
        return submitButton;
    }
    
    protected void initRootFormLayout() {
        rootFormLayout.addStyleName("");
        rootFormLayout.setEnabled(true);
        rootFormLayout.setVisible(true);
        rootFormLayout.setSpacing(true);
        rootFormLayout.setMargin(true);
        rootFormLayout.setWidth("80.0%");
        rootFormLayout.setHeight("100.0%");
    }
    
    public final FormLayout getRootFormLayout() {
        return rootFormLayout;
    }
}