package de.mekaso.logic.event;

import javax.enterprise.util.AnnotationLiteral;

public class PresenterQualifier extends AnnotationLiteral<PresenterType> implements PresenterType {
	private static final long serialVersionUID = 1L;
	private Presenter presenter;

	public PresenterQualifier(Presenter presenter) {
	      this.presenter = presenter;
	}
	
	@Override
	public Presenter value() {
		return this.presenter;
	}
}
