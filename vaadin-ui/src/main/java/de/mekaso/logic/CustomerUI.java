package de.mekaso.logic;

import javax.inject.Inject;

import com.vaadin.annotations.Title;
import com.vaadin.cdi.CDIUI;
import com.vaadin.cdi.CDIViewProvider;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

@CDIUI("")
@Title("A Vaadin web application using drools business logic")
public class CustomerUI extends UI {
	private static final long serialVersionUID = 1L;
	@Inject
    private CDIViewProvider viewProvider;
	@Override
	protected void init(VaadinRequest request) {
		Navigator navigator = new Navigator(this, this);
		navigator.addProvider(viewProvider);
	}
}