package de.mekaso.logic;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;

import de.mekaso.logic.model.Customer;

@Dependent
public class CustomerModel implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private Customer customer;
	
	@PostConstruct
	private void init() {
		this.customer = new Customer();
	}

	/**
	 * @return the customer
	 */
	protected Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	protected void setCustomer(Customer customer) {
		this.customer = customer;
	}
}
