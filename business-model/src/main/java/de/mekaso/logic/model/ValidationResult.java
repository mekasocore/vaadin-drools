package de.mekaso.logic.model;

import java.util.LinkedList;
import java.util.List;

public class ValidationResult {
	public enum ValidationState {
		SUCCESS, FAILURE
	}
	private List<ValidationMessage> messages;
	
	private ValidationResult() {
		
	}
	
	public static ValidationResult createValidationResult() {
		return new ValidationResult();
	}
	
	public ValidationResult addValidationMessage(ValidationMessage validationMessage) {
		if (this.messages == null) {
			this.messages = new LinkedList<ValidationMessage>();
		}
		this.messages.add(validationMessage);
		return this;
	}

	/**
	 * @return the messages
	 */
	public List<ValidationMessage> getMessages() {
		return messages;
	}
	
	public ValidationState getValidationState() {
		return messages == null ? ValidationState.SUCCESS : ValidationState.FAILURE;
	}
}