package de.mekaso.logic.model;

public class ValidationMessage {
	public enum Severity {
		INFO, WARNING, ERROR
	}
	private Severity severity;
	private String message;
	
	private ValidationMessage() {
		
	}
	
	public static ValidationMessage createMessage(Severity severity, String message) {
		ValidationMessage validationMessage = new ValidationMessage();
		validationMessage.setSeverity(severity);
		validationMessage.setMessage(message);
		return validationMessage;
	}
	
	/**
	 * @return the severity
	 */
	public Severity getSeverity() {
		return severity;
	}
	/**
	 * @param severity the severity to set
	 */
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}